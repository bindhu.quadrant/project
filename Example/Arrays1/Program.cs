﻿using System;

namespace Arrays1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            string[] Names = { "Bindhu", "Mounica", "Hemanth", "Sai" };
            for (int i = 0; i < Names.Length; i++)
            {
                Console.WriteLine(Names[i]);
            }
            foreach(string s in Names)
            {
                Console.WriteLine(s);
            }

            int[,] k = new int[3, 3];
            k[0, 0] = 2;
            k[0, 1] = 3;
            k[0, 2] = 4;
            k[1, 0] = 5;
            k[1, 1] = 6;
            k[1, 2] = 7;
            k[2, 0] = 8;
            k[2, 1] = 9;
            k[2, 2] = 0;
            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                {
                    Console.WriteLine(k[i,j]);
                }
            }
            Console.ReadLine();



        }
    }
}
