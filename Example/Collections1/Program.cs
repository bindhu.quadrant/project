﻿using System;
using System.Collections;

namespace Collections1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            ArrayList al = new ArrayList();
            al.Add(2);
            al.Add(3);
            al.Add("Mounica");
            al.Add('k');
            al.Add(222.3f);
            Console.WriteLine("Count: {0}", al.Count);
            Console.WriteLine("Capacity :{0}", al.Capacity);
            Console.WriteLine(al.Contains("Mounica") );
            al.Remove(2);
            Console.WriteLine(al.IndexOf('k')); 
            foreach(object obj in al)
            {
                Console.WriteLine(obj);
            }
            Console.ReadLine();


        }
    }
}
