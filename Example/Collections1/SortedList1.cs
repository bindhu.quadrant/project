﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collections1
{
    class SortedList1
    {
        public static void main()
        {
            SortedList<int, string> sl1 = new SortedList<int, string>();
            sl1.Add(3, "Three");
            sl1.Add(1, "One");
            sl1.Add(2, "Two");
            sl1.Add(4, null);
            //sl1.Add("Three", 3); //Error: cannot convert string to int key

            // the following will throw run-time exceptions
            //sl1.Add(1, null); // duplicate key
            //sl1.Add(null, "Five");// key cannot be null

            SortedList<string, int> sl2 = new SortedList<string, int>();
            sl2.Add("I", 1);
            sl2.Add("II", 2);
            sl2.Add("III", 3);
            //sl2.Add(null, 4); // run-time exception: key cannot be null 
            SortedList<int, string> mySL = new SortedList<int, string>()
                                    {
                                        {3, "Three"},
                                        {1, "One"},
                                        {2, "Two"}
                 };
            foreach (KeyValuePair<int, string> kvp in sl1)
                Console.WriteLine("key: {0}, value: {1}", kvp.Key, kvp.Value);
            Console.ReadLine();
       
}
    }
}
